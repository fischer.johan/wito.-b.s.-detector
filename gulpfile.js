/**
 * @typedef {import('stream').Transform} Transform
 */

const fs = require("fs");
const axios = require("axios").default;
const readlineSync = require("readline-sync");
const gulp = require("gulp");
const clean = require("gulp-clean");
const cleanhtml = require("gulp-cleanhtml");
const jeditor = require("gulp-json-editor");
const merge = require("merge-stream");
const webpack = require("webpack-stream");
const TerserWebpackPlugin = require("terser-webpack-plugin");
const zip = require("gulp-zip");

const BUILD_CONFIG_FILE = "./ext/config/buildConfig.json";
const buildConfig = require(BUILD_CONFIG_FILE);

const APP_CONFIG_FILE = `./ext/config/config.json`;

const manifest = require("./ext/manifest.json");

const Global = {
    selectedApplication: {},
    selectedEnvironment: "",
    customEnvironmentUrl: null,
};

const KNOWN_ENVIRONMENTS = ["Production", "Staging", "Custom"];

module.exports.default = gulp.series(
    gulpSelectTask,
    gulpClean,
    gulpStatic,
    gulpLoadGeneralIcons,
    gulpLoadLevelIcons,
    gulpLoadCategoryIcons,
    gulpLoadEditionIcons,
    gulpApplicationConfig,
    gulpManifest,
    gulpHtml,
    gulpScripts,
    gulpZip,
    gulpSuccessInfo
);

/**
 * Queries the list of available applications from AWS Lambda
 * and lets the user choose the application and the environment
 * which should be targeted by the following build-process.
 *
 * Examples for applications: "MyGuard", "LRF - MyGuard", "Corsair"
 * Examples for environments: "Production", "Staging", "Custom"
 *
 * @returns {Promise}
 */
async function gulpSelectTask() {
    const response = await axios.get(
        "https://y5apldnvr7.execute-api.eu-west-1.amazonaws.com/staging/applications"
    );
    const applicationsData = response.data;

    const applicationSelectionIndex = readlineSync.keyInSelect(
        applicationsData.map(app => app.name),
        "Choose application to build"
    );
    if (applicationSelectionIndex === -1) {
        process.exit(0);
    }

    Global.selectedApplication = applicationsData[applicationSelectionIndex];

    const environmentSelectionIndex = readlineSync.keyInSelect(
        KNOWN_ENVIRONMENTS,
        "Choose environment",
        { defaultInput: "Staging" }
    );
    if (environmentSelectionIndex === -1) {
        process.exit(0);
    }

    Global.selectedEnvironment = KNOWN_ENVIRONMENTS[environmentSelectionIndex];

    if (Global.selectedEnvironment === "Custom") {
        Global.customEnvironmentUrl =
            readlineSync.question("Enter custom url: ");
    }
}

/**
 * Initially removes all files from the "build" folder
 *
 * @returns {Transform}
 */
function gulpClean() {
    return gulp.src("build/*", { read: false }).pipe(clean());
}

/**
 * Moves all those static files into "build/ext"
 * which are needed but not processed in a later step
 *
 * @returns {NodeJS.ReadWriteStream}
 */
function gulpStatic() {
    return gulp
        .src(
            [
                "ext/**",
                "!ext/icons/**",
                "!ext/config/**",
                "!ext/**/*.html",
                "!ext/**/*.js",
                "!ext/manifest.json",
            ],
            { nodir: true }
        )
        .pipe(gulp.dest("build/ext"));
}

/**
 * Processes all generic icons
 *
 * @returns {NodeJS.ReadWriteStream}
 */
function gulpLoadGeneralIcons() {
    return gulp
        .src([`ext/icons/*.png`, `ext/icons/*.svg`])
        .pipe(gulp.dest("build/ext/icons"));
}

/**
 * Processes all level icons
 *
 * @returns {NodeJS.ReadWriteStream}
 */
function gulpLoadLevelIcons() {
    return gulp
        .src(`ext/icons/levels/*`)
        .pipe(gulp.dest("build/ext/icons/levels"));
}

/**
 * Processes the icons of those categories
 * which are listed in config.activeLabels of the selected application
 *
 * @returns {NodeJS.ReadWriteStream}
 */
function gulpLoadCategoryIcons() {
    const { activeLabels } = Global.selectedApplication.config;

    const categoryPaths = activeLabels
        .map(labelID => {
            const filename = buildConfig.categoryToIcon[labelID]
                .split(/(\\|\/)/g)
                .pop();
            const categoryPath = `ext/icons/categories/${filename}`;

            if (fs.existsSync(categoryPath)) {
                return categoryPath;
            }

            console.warn(
                `The category icon for ${labelID} does not exist at ${categoryPath}`
            );
            return null;
        })
        .filter(path => path != null);

    return gulp
        .src(categoryPaths)
        .pipe(gulp.dest("build/ext/icons/categories"));
}

/**
 * Processes the images in icons/editions
 *
 * @param {Function} done
 *      which will be invoked if no icons could be found
 *      for the edition of the given application
 *
 * @returns {NodeJS.ReadWriteStream|null}
 */
function gulpLoadEditionIcons(done) {
    const { edition } = Global.selectedApplication.config;

    const applicationIconPath = `ext/icons/editions/${edition.toLowerCase()}`;
    if (!fs.existsSync(applicationIconPath)) {
        const message = `Warning: The icon path "${applicationIconPath}" for edition ${edition} does not exist - no edition-icons will be shipped`;
        console.warn(message);
        done();
        return null;
    }

    return gulp
        .src(`${applicationIconPath}/*`)
        .pipe(gulp.dest(`build/${applicationIconPath}`));
}

/**
 * Processes the config-file
 *
 * @throws
 *      e.g. in case of missing files
 * @returns {NodeJS.ReadWriteStream}
 */
function gulpApplicationConfig() {
    const application = Global.selectedApplication;

    if (!fs.existsSync(APP_CONFIG_FILE)) {
        const message = `Warning the config path "${APP_CONFIG_FILE}" for application ${application.name} does not exist`;
        console.error(message);
        throw new Error(message);
    }

    const environmentUrl =
        Global.selectedEnvironment === "Custom"
            ? Global.customEnvironmentUrl
            : application.config.endpoints[
                  Global.selectedEnvironment.toLowerCase()
              ];

    const activeCategoryIcons = {};
    application.config.activeLabels.forEach(labelID => {
        try {
            activeCategoryIcons[labelID] = buildConfig.categoryToIcon[labelID];
        } catch (error) {
            const message = `The active category ${labelID} does not have a matching icon specified in ${BUILD_CONFIG_FILE}.`;
            console.log(message);
            throw new Error(message);
        }
    });

    return gulp
        .src(APP_CONFIG_FILE)
        .pipe(
            jeditor(json => {
                json.remote = { BASE_URL: environmentUrl };
                json.activeLabels = application.config.activeLabels;
                json.categoryToIcon = activeCategoryIcons;
                json.edition = application.config.edition;
                return json;
            })
        )
        .pipe(gulp.dest("build/ext/config"));
}

/**
 * Processes manifest.json
 *
 * Note: In production mode the entry-point to the content-script
 *       will be changed from "index-initmodule.js" to "index.js".
 *       Some entries of "web_accessible_resources" will be removed.
 *
 * @returns {NodeJS.ReadWriteStream}
 */
function gulpManifest() {
    const _rewriteManifest = manifest => {
        if (Global.selectedEnvironment === "Production") {
            manifest.name = Global.selectedApplication.name;
            manifest.content_scripts.forEach(item => {
                item.js = item.js.map(filename =>
                    filename === "content-script/index-initmodule.js"
                        ? "content-script/index.js"
                        : filename
                );
            });
            manifest.web_accessible_resources =
                manifest.web_accessible_resources.filter(
                    glob =>
                        !["content-script/*", "general-utils.js"].includes(glob)
                );
        } else {
            manifest.name = `${Global.selectedApplication.name} (${Global.selectedEnvironment})`;
        }
        return manifest;
    };

    return gulp
        .src("ext/manifest.json")
        .pipe(jeditor(_rewriteManifest))
        .pipe(gulp.dest("build/ext"));
}

/**
 * Processes HTML-files
 *
 * @returns {NodeJS.ReadWriteStream}
 */
function gulpHtml() {
    return gulp
        .src("ext/**/*.html")
        .pipe(cleanhtml())
        .pipe(gulp.dest("build/ext"));
}

/**
 * Processes JavaScript-code
 *
 * Note: In production mode the files will be minified and
 *       any debug-logging will be removed.
 *
 * @returns {NodeJS.ReadWriteStream}
 */
function gulpScripts() {
    if (Global.selectedEnvironment === "Production") {
        console.log("Removing logs and obfuscating code...");

        const terserOptions = {
            toplevel: true,
            compress: { drop_console: true },
        };

        const streams = {
            copyJQuery: gulp
                .src([
                    "ext/content-script/jquery/jquery-3.1.1.js",
                    "ext/content-script/jquery/mutation-summary.js",
                    "ext/content-script/jquery/jquery.mutation-summary.js",
                ])
                .pipe(gulp.dest("build/ext/content-script/jquery")),

            transformScripts: _transformWithWebpackFromES6ModuleIntoScript(
                [
                    "background-script/index.js",
                    "content-script/index.js",
                    "options-popup/index.js",
                ],
                { terserOptions }
            ),
        };

        return merge(Object.values(streams));
    }

    return gulp.src(["ext/**/*.js"]).pipe(gulp.dest("build/ext"));
}

/**
 * @param {string[]} paths
 * @param {object} [optionsBag]
 * @param {object} [optionsBag.terserOptions]
 *
 * @returns {NodeJS.ReadWriteStream}
 */
function _transformWithWebpackFromES6ModuleIntoScript(paths, optionsBag) {
    const { terserOptions } = optionsBag || {};

    /** @type {{ [index: string]: string}} entry */
    const entry = {};
    paths.forEach(key => {
        entry[key] = `./ext/${key}`;
    });
    const mode = "production";
    const optimization = {
        minimize: true,
        minimizer: [new TerserWebpackPlugin({ terserOptions })],
    };
    const output = { filename: "[name]" };

    // @ts-ignore
    return webpack({ entry, mode, optimization, output }).pipe(
        gulp.dest("build/ext")
    );
}

/**
 * Processes prepared "build" folder and
 * creates an archive in "dist" folder
 *
 * @returns {NodeJS.ReadWriteStream}
 */
function gulpZip() {
    const distFileName = manifest.name + " v" + manifest.version + ".zip";

    return gulp
        .src(["build/**"])
        .pipe(zip(distFileName))
        .pipe(gulp.dest("dist"));
}

/**
 * Show success message
 *
 * @param {Function} done
 */
function gulpSuccessInfo(done) {
    const message = `Successfully built application "${Global.selectedApplication.name}" for environment "${Global.selectedEnvironment}"`;
    console.log(message);
    done();
}
