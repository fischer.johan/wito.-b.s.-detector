/*!
 * B.S. Detector v0.2.7 (http://bsdetector.tech)
 * Copyright 2016 The B.S. Detector Authors (https://github.com/selfagency/bs-detector/graphs/contributors)
 * Licensed under LGPL-3.0 (https://github.com/selfagency/bs-detector/blob/master/LICENSE)
 */

/*
  Utility functions needed by the front and backends of the extension
  */

export {
    getTabId,
    emailToDomain,
    divideArray,
    getDomainName,
    url2Domain,
    url2Path,
    cleanHref,
    isUrl,
    isSubUrl,
    isSamePosition,
    removeDuplicates,
    getBrowser,
};

function getTabId() {}

function emailToDomain(email) {
    return email.split("@")[1];
}

function divideArray(source, filter) {
    const arr1 = [],
        arr2 = [];

    source.forEach(element => {
        if (filter(element)) {
            arr1.push(element);
        } else {
            arr2.push(element);
        }
    });

    return [arr1, arr2];
}

function getDomainName(url) {
    // const domain = url2Domain(url);
    return url.split(".")[0];
}

function url2Domain(url) {
    if (url) {
        url = url.toString().replace(/^(?:https?|ftp):\/\//i, "");
        url = url.toString().replace(/^www\./i, "");
        url = url.toString().replace(/\/.*/, "");
        return url.toLowerCase();
    }
}

function url2Path(url) {
    if (url) {
        url = url.toString().replace(/^(?:https?|ftp):\/\//i, "");
        url = url.toString().replace(/^www\./i, "");
        url = url.toString().split("?")[0];
        url = url.toString().replace(/\/$/, "");
        url = url.toString().replace(/^\//, "");
        return url;
    }
}

function cleanHref(href) {
    if (href) {
        href = href.toString().replace(/^(?:https?|ftp):\/\//i, "");
        href = href.toString().replace(/\/$/, "");
        return href;
    }
}

function isUrl(url) {
    const isUrlRegExp = new RegExp(
        "/^(http://www.|https://www.|http://|https://)?[a-z0-9]+([-.]{1}[a-z0-9]+)*.[a-z]{2,5}(:[0-9]{1,5})?(/.*)?$"
    );
    return isUrlRegExp.test(url);
}

function isSubUrl(currentUrl, urlToCheck) {
    return urlToCheck.startsWith(currentUrl);
}

function isSamePosition(element1, element2) {
    return (
        element1.offset().top === element2.offset().top &&
        element1.offset().left === element2.offset().left
    );
}

/**
 * @param {Array} list e.g. [1, 65, 2, 7, 2, 4, 3, 2]
 * @returns e.g. [1, 65, 2, 7, 4, 3]
 */
function removeDuplicates(list) {
    return Array.from(new Set(list));
}

function getBrowser() {
    // @ts-ignore
    return typeof chrome === "undefined" ? browser : chrome;
}
