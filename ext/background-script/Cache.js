const Cache = {
    cacheObj: {},

    hasExpired(cachedTime, domain) {
        const KEEP_IN_CACHE_TIME_MS = 60 * 60 * 1000;
        return Date.now() - cachedTime > KEEP_IN_CACHE_TIME_MS;
    },

    contains(domain) {
        return (
            domain in this.cacheObj &&
            !this.hasExpired(this.cacheObj[domain].cacheDate, domain)
        );
    },

    get(domain) {
        return this.cacheObj[domain];
    },

    insert(domain, value) {
        this.cacheObj[domain] = value;
        this.cacheObj[domain].cacheDate = Date.now();
    },
};

export default Cache;
