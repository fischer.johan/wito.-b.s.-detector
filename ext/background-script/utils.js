/*!
 * B.S. Detector v0.2.7 (http://bsdetector.tech)
 * Copyright 2016 The B.S. Detector Authors (https://github.com/selfagency/bs-detector/graphs/contributors)
 * Licensed under LGPL-3.0 (https://github.com/selfagency/bs-detector/blob/master/LICENSE)
 */

import { url2Domain, url2Path } from "../general-utils.js";

import Cache from "./Cache.js";
import CacheTraverse from "./CacheTraverse.js";

export {
    setConfig,
    getConfig,
    getBaseUrl,
    xhReq,
    sendWitoLabelsRequest,
    sendUrlInfoRequest,
    getDomainInfo,
    unshortenMeAPI,
    expandUrlAPI,
    expandLinks,
    expandLink,
};

const Global = {
    config: {},
    baseUrl: "",
};

function setConfig(data) {
    Global.config = data;
    Global.baseUrl = data.remote.BASE_URL;
}

function getConfig() {
    return Global.config;
}

function getBaseUrl() {
    return Global.baseUrl;
}

function xhReq(url, METHOD = "GET", data = null) {
    return new Promise((resolve, reject) => {
        var xhr = new XMLHttpRequest();
        xhr.overrideMimeType("application/json");
        xhr.timeout = 3000;
        xhr.open(METHOD, url, true);

        xhr.onload = function () {
            if (xhr.status === 200) {
                resolve(xhr.responseText);
            } else {
                console.error("Rejecting request", url);
                reject(xhr.responseText);
            }
        };

        xhr.ontimeout = function () {
            reject({ success: false });
        };

        xhr.send(data);
    });
}

async function sendWitoLabelsRequest() {
    const endpoint = Global.baseUrl + "/categories?type=default_categories";
    const response = await xhReq(endpoint);
    return JSON.parse(response);
}

async function sendUrlInfoRequest(urlsToFetch) {
    const endpoint = Global.baseUrl + "/sites";
    const data = JSON.stringify({ urlArr: urlsToFetch });
    console.log("Sending data", data);
    const response = await xhReq(endpoint, "POST", data);
    return response;
}

async function getDomainInfo(urls) {
    const urlsToFetch = urls.filter(url => !Cache.contains(url2Domain(url)));
    if (urlsToFetch.length > 0) {
        const jsonResponse = JSON.parse(await sendUrlInfoRequest(urlsToFetch));

        urlsToFetch.forEach(url => Cache.insert(url2Domain(url), {}));
        jsonResponse.forEach(responseItem =>
            Cache.insert(url2Domain(responseItem.siteId), responseItem)
        );
    }

    return urls
        .map(url => CacheTraverse.getCachedSiteInfo(Cache.cacheObj, url))
        .filter(urlInfo => urlInfo.categories);
}

async function unshortenMeAPI(urlToExpand) {
    console.log("UnshortenME API");

    var requestUrl =
        "https://unshorten.me/json/" + encodeURIComponent(urlToExpand);
    try {
        let response = JSON.parse(await xhReq(requestUrl));
        console.log("Response unshortenME", response);
        if (response.success) {
            return {
                success: true,
                requested_url: urlToExpand,
                resolved_url: url2Path(
                    decodeURIComponent(response.resolved_url)
                ),
            };
        }
    } catch (error) {
        console.log(error);
    }

    return {
        success: false,
        requested_url: "",
        resolved_url: "",
    };
}

async function expandUrlAPI(urlToExpand) {
    console.log("ExpandUrl API");
    const requestUrl =
        "http://expandurl.com/api/v1/?url=" +
        encodeURIComponent(urlToExpand) +
        "&format=json&detailed=true";
    try {
        const response = await xhReq(requestUrl);
        console.log("Response expandUrlAPI", JSON.parse(response));
        return {
            success: true,
            requested_url: urlToExpand,
            resolved_url: url2Path(
                JSON.parse(response).rel_meta_refresh[0].url
            ),
        };
    } catch (error) {
        console.log(error);
        return {
            success: false,
            requested_url: "",
            resolved_url: "",
        };
    }
}

function expandLinks(shortLinks) {
    return Promise.all(
        shortLinks.filter(link => link).map(link => expandLink(link))
    );
}

async function expandLink(url) {
    let response = await unshortenMeAPI(url);
    if (!response.success) {
        response = await expandUrlAPI(url);
    }

    console.log("Returning expand link", response);
    return response;
}
