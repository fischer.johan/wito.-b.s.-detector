/*!
 * B.S. Detector v0.2.7 (http://bsdetector.tech)
 * Copyright 2016 The B.S. Detector Authors (https://github.com/selfagency/bs-detector/graphs/contributors)
 * Licensed under LGPL-3.0 (https://github.com/selfagency/bs-detector/blob/master/LICENSE)
 */

import { getBrowser } from "../general-utils.js";

import { setConfig, xhReq } from "./utils.js";
import handleOnMessage from "./handleOnMessage.js";

init();

function init() {
    _setupEventListeners();

    xhReq(getBrowser().runtime.getURL("../config/config.json")).then(file => {
        setConfig(JSON.parse(file));
    });
}

function _setupEventListeners() {
    /**
     * Handle messages from options-popup or content-script
     */
    getBrowser().runtime.onMessage.addListener(
        (request, sender, sendResponse) => {
            handleOnMessage(request).then(response => sendResponse(response));
            return true;
        }
    );

    // /**
    //  * Toggle display of the warning UI when the pageAction is clicked
    //  */
    // getBrowser().browserAction.onClicked.addListener((tab) => {
    //     "use strict";
    //     xhReq(getBrowser().extension.getURL("./html/popup.html")).then((htmlFile) => {
    //         console.log("togglePopup");
    //         const response = { operation: "togglePopup", data: htmlFile };
    //         getBrowser().tabs.sendMessage(tab.id, response);
    //     });
    //     return true;
    // });

    /**
     * Refresh language setting in content-script of current page
     * if language was changed in options-popup
     */
    getBrowser().storage.onChanged.addListener((changes, namespace) => {
        console.log("storage changed");

        if (Object.keys(changes).includes("translation_selected")) {
            getBrowser().tabs.query(
                { active: true, currentWindow: true },
                tabs => {
                    if (!tabs[0].id) {
                        return;
                    }
                    getBrowser().tabs.sendMessage(tabs[0].id, {
                        operation: "languageChange",
                    });
                }
            );
        }
    });
}
