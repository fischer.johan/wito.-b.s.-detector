import { removeDuplicates, getBrowser } from "../general-utils.js";

import {
    expandLinks,
    getDomainInfo,
    sendWitoLabelsRequest,
    getConfig,
} from "./utils.js";

export default handleOnMessage;

const SHORTS = [
    "bit.do",
    "bit.ly",
    "cutt.us",
    "goo.gl",
    "ht.ly",
    "is.gd",
    "ow.ly",
    "po.st",
    "tinyurl.com",
    "tr.im",
    "trib.al",
    "u.to",
    "v.gd",
    "x.co",
    "t.co",
];

async function handleOnMessage(request) {
    console.log(request);

    const handlers = {
        getShorts() {
            return SHORTS;
        },

        async expandLinks() {
            const response = await expandLinks(request.shortLinks);
            console.log("Sending response from expandlinks", response);
            return { expandedLinks: response };
        },

        async getUrlInfo() {
            const response = await getDomainInfo(
                removeDuplicates(request.urls)
            );
            console.log("Sending response from url info", response);
            return response;
        },

        async togglePopup() {
            await getBrowser().tabs.getSelected(null, tabs => {
                console.log("Tabs", tabs);
                getBrowser().tabs.sendMessage(tabs.id, request);
            });
        },

        rateLabels() {
            console.log("request: ", request.data);
        },

        async getWitoLabels() {
            const response = await sendWitoLabelsRequest();
            return response;
        },

        storeValue() {
            const setKey = request.key;
            const value = request.value;
            getBrowser().storage.local.set({ [setKey]: value }, () => {
                console.log("Value is set to " + value);
            });
        },

        getStoredValue() {
            const getKey = request.key;
            return new Promise(resolve => {
                getBrowser().storage.local.get([getKey], result => {
                    resolve(result[getKey]);
                });
            });
        },

        getConfigFile() {
            return getConfig();
        },
    };

    if (typeof handlers[request.operation] === "function") {
        const result = await handlers[request.operation]();
        return result;
    }

    console.log("Invalid request", { operation: request.operation });
    return null;
}
