import { url2Path, url2Domain, isSubUrl } from "../general-utils.js";

import {
    ShortLinkHandler,
    FBLinkHandler,
    EmailLinkHandler,
    PhoneNumberHandler,
    StandardLinkHandler,
} from "./linkHandlers/index.js";

import {
    CustomEmailPageHandler,
    GmailPageInfo,
    OutlookPageInfo,
    StandardPageHandler,
} from "./pageHandlers/index.js";

import { ManageModal, ManageBannerModal } from "./modal/index.js";

import * as BackgroundAPI from "./BackgroundAPI.js";
import * as WitoHTML from "./WitoHTML.js";

export default initWitoAsync;

/**
 * @param {object} [optionsBag]
 * @param {boolean} [optionsBag.clearOldStateFirst]
 */
async function initWitoAsync(optionsBag) {
    if (optionsBag) {
        if (optionsBag.clearOldStateFirst === true) {
            _clearLinks();
            _clearModals();
        }
    }

    const language = await BackgroundAPI.getStoredLanguage();
    const siteInfo = await _getSiteInfo(window.location.href);
    const witoInfo = await _getWitoInfo(language);

    if (siteInfo.siteLabeled) {
        _flagSite(siteInfo, witoInfo);
    }
    _handlePage(siteInfo, witoInfo);
    _startMutationListener(siteInfo, witoInfo);
}

async function _getSiteInfo(url) {
    const domainInfo = await BackgroundAPI.getDomainInfo(url);
    const siteInfo = _createSiteInfo(domainInfo, url);
    console.log("Site info", siteInfo);
    return siteInfo;
}

function _createSiteInfo(domainInfo, url) {
    return {
        siteLabeled: domainInfo ? true : false,
        siteUrl: url2Path(url),
        siteDomain: url2Domain(url),
        categories: domainInfo ? domainInfo.categories : "",
        labeledUrl: domainInfo ? domainInfo.labeledUrl : "",
    };
}

async function _getWitoInfo(language) {
    const allWitoInfo = await BackgroundAPI.getWitoLabels();
    const configFile = await BackgroundAPI.getConfigFile();

    WitoHTML.setSetup({
        witoClassificationInfo: {
            categories: await _filterActiveLabels(
                allWitoInfo.categories,
                language,
                configFile
            ),
            levels: _filterActiveLanguage(allWitoInfo.levels, language),
        },
        edition: configFile.edition,
        categoryToIcon: configFile.categoryToIcon,
    });

    const shorts = await BackgroundAPI.getShorts();
    const witoInfo = _createWitoInfo(shorts);
    return witoInfo;
}

async function _filterActiveLabels(labels, language, configFile) {
    const dbCategoryIds = Object.keys(labels);
    const activeLabels = configFile.activeLabels.filter(category => {
        if (dbCategoryIds.some(dbCategory => dbCategory === category)) {
            return true;
        } else {
            console.warn(
                "Warning: The active category does not exist in the database"
            );
            return false;
        }
    });
    return activeLabels.reduce((result, key) => {
        result[key] = labels[key][language];
        return result;
    }, {});
}

function _filterActiveLanguage(levels, language) {
    return Object.keys(levels).reduce((result, key) => {
        result[key] = levels[key][language];
        return result;
    }, {});
}

function _createWitoInfo(shorts) {
    return {
        shorts: shorts,
    };
}

function _flagSite(siteInfo, witoInfo) {
    const labels = Array.isArray(siteInfo.categories)
        ? siteInfo.categories
        : [siteInfo.categories];
    console.log(labels);
    const filteredLabels = labels.filter(WitoHTML.isActiveCategory);
    if (filteredLabels.length > 0) {
        WitoHTML.flagSite(filteredLabels);
    }
}

function _handlePage(siteInfo, witoInfo) {
    const linkHandlers = [
        new ShortLinkHandler(witoInfo.shorts),
        new FBLinkHandler(),
        new EmailLinkHandler(),
        new PhoneNumberHandler(),
        new StandardLinkHandler(),
    ];

    const pageHandlers = [
        new CustomEmailPageHandler(new GmailPageInfo(), siteInfo),
        new CustomEmailPageHandler(new OutlookPageInfo(), siteInfo),
        new StandardPageHandler(linkHandlers, siteInfo),
    ];

    const linkPromises = pageHandlers
        .filter(handler => handler.matches(siteInfo))
        .flatMap(pageHandler => pageHandler.getLinks());

    Promise.all(linkPromises).then(links => {
        const filteredLinks = _removeParentLinks(
            _filterOnCategory(links.flat()),
            siteInfo
        );
        console.log("Number of new flagged links: ", filteredLinks.length);
        _flagLinks(filteredLinks, siteInfo);
    });
}

function _removeParentLinks(links, siteInfo) {
    return links.filter(
        link =>
            !(
                siteInfo.siteLabeled &&
                isSubUrl(link["wito-info"].labeledUrl, siteInfo.labeledUrl)
            )
    );
}

function _filterOnCategory(links) {
    const typeFilter = witoLink => {
        const categories = WitoHTML.getTypeFromElement(witoLink);
        return WitoHTML.hasActiveCategory(categories);
    };
    return links.filter(typeFilter);
}

function _flagLinks(flaggedLinks, siteInfo) {
    const extractor = elem => [elem["element"], elem["wito-info"].categories];
    WitoHTML.setLinkFlaggedAttribute(flaggedLinks, extractor);
    const flaggedElements = flaggedLinks.map(link => link["element"]);
    WitoHTML.flagLinks(flaggedElements, WitoHTML.getFlagLinkAnchor(siteInfo));
}

function _mutationChanged(siteInfo, witoInfo) {
    console.log("Mutation callback called");
    WitoHTML.disconnectMutationObserver();

    if (siteInfo.siteUrl === url2Path(window.location.href)) {
        _handlePage(siteInfo, witoInfo);
        _startMutationListener(siteInfo, witoInfo);
    } else {
        initWitoAsync({ clearOldStateFirst: true });
    }
}

function _clearLinks() {
    console.log("clear links");
    WitoHTML.clearLinks();
}

function _clearModals() {
    ManageModal.resetModal();
    ManageBannerModal.resetModal();
}

function _startMutationListener(siteInfo, witoInfo) {
    WitoHTML.connectMutationObserver(() =>
        _mutationChanged(siteInfo, witoInfo)
    );
}
