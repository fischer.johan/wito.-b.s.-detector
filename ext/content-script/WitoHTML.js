import { getDomainName } from "../general-utils.js";
import * as WitoFlagging from "./WitoFlagging.js";
import { showBanner } from "./banner.js";

export {
    getSetup,
    setSetup,
    setWindowSize,
    getWindowSize,
    getLinkElementsNotChecked,
    getGmailElements,
    getOutlookElements,
    setLinkCheckedAttribute,
    setLinkFlaggedAttribute,
    flagLinks,
    getFlagLinkAnchor,
    flagSite,
    getTypeFromElement,
    hasActiveCategory,
    isActiveCategory,
    connectMutationObserver,
    disconnectMutationObserver,
    clearLinks,
};

const Global = {
    witoClassificationInfo: {},
    edition: "myguard",
    categoryToIcon: {},

    windowSize: {
        width: null,
        height: null,
    },
};

function getSetup() {
    const { witoClassificationInfo, edition, categoryToIcon } = Global;
    return {
        witoClassificationInfo,
        edition,
        categoryToIcon,
    };
}

function setSetup({ witoClassificationInfo, edition, categoryToIcon }) {
    if (witoClassificationInfo !== undefined) {
        Global.witoClassificationInfo = witoClassificationInfo;
    }
    if (edition !== undefined) {
        Global.edition = edition;
    }
    if (categoryToIcon !== undefined) {
        Global.categoryToIcon = categoryToIcon;
    }
}

function getWindowSize() {
    return Global.windowSize;
}

function setWindowSize() {
    // https://javascript.info/size-and-scroll-window
    Global.windowSize.width = Math.max(
        document.body.scrollWidth,
        document.documentElement.scrollWidth,
        document.body.offsetWidth,
        document.documentElement.offsetWidth,
        document.body.clientWidth,
        document.documentElement.clientWidth
    );
    Global.windowSize.height = Math.max(
        document.body.scrollHeight,
        document.documentElement.scrollHeight,
        document.body.offsetHeight,
        document.documentElement.offsetHeight,
        document.body.clientHeight,
        document.documentElement.clientHeight
    );
}

// function getAllLinkElements() {
//     const allLinks = $('a[href]:not([href^="#"])');
//     return allLinks.toArray();
// }

function getLinkElementsNotChecked() {
    let links = $("a[href]").not('[href^="#"]').not("[wito-href]");
    // @ts-ignore
    links = links.toArray();
    return links;
}

function getGmailElements() {
    let links = $('span[email*="@"]').not("[wito-href]");
    // @ts-ignore
    links = links.toArray();
    return links;
}

function getOutlookElements() {
    let links = $('span[title*="@"]').not("[wito-href]");
    // @ts-ignore
    links = links.toArray();
    return links;
}

function _setAttribute(links, attribute, extractor) {
    links.forEach(link => {
        const [element, value] = extractor(link);
        $(element).attr(attribute, value);
    });
}

function setLinkCheckedAttribute(links, extractor) {
    const keyAttribute = "wito-href";
    _setAttribute(links, keyAttribute, extractor);
}

function setLinkFlaggedAttribute(links, extractor) {
    const keyAttribute = "wito-flagged";
    _setAttribute(links, keyAttribute, extractor);
}

function flagLinks(links, anchorExtract) {
    links
        .filter(link => link.getAttribute("wito-flagged"))
        .map(link => flagLink(link, anchorExtract));
}

function flagLink(element, anchorExtract) {
    const linkAnchor = anchorExtract(element);
    let flaggedLabels = element.getAttribute("wito-flagged").split(",");
    const labelInfo = getLabelInfo(flaggedLabels);
    WitoFlagging.flagLink($(element), linkAnchor, labelInfo);
}

function getFlagLinkAnchor(siteInfo) {
    return link => {
        switch (getDomainName(siteInfo.siteDomain)) {
            case "twitter":
                return $(link);
            //return $($(link).closest('article'));

            case "_facebook":
                if ($(link).parents(".mtm").length > 0) {
                    return $($(link).closest(".mtm"));
                }
                break;

            case "_google":
                const gParents = $(link).parents(".rc");
                if (gParents.length > 0) {
                    return $(link); //.closest('.rc');
                }
                break;

            default:
                break;
        }

        return $(link);
    };
}

function flagSite(types) {
    const labelInfo = getLabelInfo(types);
    showBanner(labelInfo);
}

function getTypeFromElement(element) {
    return Array.isArray(element["wito-info"].categories)
        ? element["wito-info"].categories
        : element["wito-info"].categories;
}

function hasActiveCategory(categories) {
    return categories.some(
        category =>
            Global.witoClassificationInfo.categories[category] !== undefined
    );
}

function isActiveCategory(category) {
    return Global.witoClassificationInfo.categories[category] !== undefined;
}

function getLabelInfo(categoryIds) {
    return categoryIds
        .map(categoryId => {
            const categoryDetails =
                Global.witoClassificationInfo.categories[categoryId];
            if (categoryDetails) {
                return { ...categoryDetails, id: categoryId };
            }
            return null;
        })
        .filter(info => info);
}

function connectMutationObserver(callback) {
    const observerRoot = $("body");
    const observerFilter = [{ element: "div" }];
    // @ts-ignore
    observerRoot.mutationSummary("connect", callback, observerFilter);
}

function disconnectMutationObserver() {
    const observerRoot = $("body");
    // @ts-ignore
    observerRoot.mutationSummary("disconnect");
}

function clearLinks() {
    $("[wito-href]").each(function () {
        $(this).removeAttr("wito-href wito-flagged");
    });
    $("div.wito-alert-popup").remove();
    $("div.wito-alert").remove();
}
