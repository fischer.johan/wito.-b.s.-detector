import * as ManageModal from "./modal/ManageModal.js";
import * as WitoAnimations from "./WitoAnimations.js";

import { levelToIcon } from "./levels.js";

export { flagLink };

const HAS_JQUERY = typeof $ !== "undefined";

function flagLink(element, linkAnchor, labelInfo) {
    const iconsHtml = _getIcons(labelInfo.map(label => label.level));

    if (HAS_JQUERY) {
        if (linkAnchor.hasClass("wito-alert-popup")) {
            return;
        }

        const flagHtml = $(`<div class="wito-alert-popup">${iconsHtml}</div>`);

        linkAnchor.before(flagHtml);

        const popupDimens = {
            width: $(".wito-alert-icon").width(),
            height: $(".wito-alert-icon").height(),
        };
        _setPopupPosition(element, flagHtml, popupDimens);
        _setPopupModal(element, flagHtml, labelInfo, popupDimens.width);
        _setPopupIcon(flagHtml);
    } else {
        const isWitoAlertPopup = linkAnchor
            .getAttribute("class")
            .split(" ")
            .includes("wito-alert-popup");
        if (isWitoAlertPopup) {
            return;
        }

        const flagHtml = document.createElement("div");
        flagHtml.setAttribute("class", "wito-alert-popup");
        flagHtml.innerHTML = iconsHtml;

        linkAnchor.parentNode.insertBefore(flagHtml, linkAnchor);

        const witoAlertIcons =
            document.getElementsByClassName("wito-alert-icon");
        if (witoAlertIcons.length > 0) {
            const { width, height } = witoAlertIcons[0].getBoundingClientRect();
            _setPopupPosition(element, flagHtml, {
                width,
                height,
            });
            _setPopupModal(element, flagHtml, labelInfo, width);
            _setPopupIcon(flagHtml);
        }
    }
}

function _setPopupPosition(element, flagHtml, popupDimens) {
    const positionedElement = _getClosestPositionedElement(element);

    const _update = () => {
        const css = _calculatePopupPosition(
            element,
            positionedElement,
            popupDimens
        );

        if (HAS_JQUERY) {
            flagHtml.css(css);
        } else {
            const cssString = Object.keys(css)
                .map(key => `${key}: ${css[key]};`)
                .join(" ");
            flagHtml.setAttribute("style", cssString);
        }
    };

    _update();

    if (HAS_JQUERY) {
        $(window).resize(_update);
    } else {
        window.addEventListener("resize", _update);
    }
}

function _setPopupModal(element, flagHtml, labelInfo, popupWidth) {
    flagHtml.mouseenter(() => {
        ManageModal.onPopupMouseEnter(
            element,
            labelInfo,
            flagHtml.offset(),
            popupWidth
        );
    });
    flagHtml.mouseleave(() => {
        ManageModal.onPopupMouseLeave(element);
    });
}

function _setPopupIcon(flagHtml) {
    const iconElements = flagHtml.children().toArray();

    WitoAnimations.setDefaultIconState(iconElements);
    WitoAnimations.animatePopup(iconElements);
}

/**
 * @param {string[]|number[]} levels
 *      e.g. [1, 3]
 *
 * @returns {string}
 *      e.g. `<img class="wito-alert-icon" src="..."/><img class="wito-alert-icon" src="..."/>`
 */
function _getIcons(levels) {
    const uniqueLevels = [...new Set(levels)];
    return uniqueLevels
        .map(level => {
            const iconUrl = levelToIcon(Number(level));
            return `<img class="wito-alert-icon" src="${iconUrl}" />`;
        })
        .join("");
}

function _popupFarFromElement(link, nearestPositionedElement) {
    const parentWidth = nearestPositionedElement.width();
    const linkRight = link.width() + link.position().left;
    return parentWidth - linkRight > 40;
}

function _getPopupAnchorPosition(link) {
    const anchorCandidates = link.children(":header");
    if (anchorCandidates.length === 0) {
        return {
            top: link.position().top,
            left: link.position().left,
            width: link.outerWidth(),
        };
    }
    let anchor = anchorCandidates.first();
    let topPadding = parseInt(anchor.css("padding-top"));
    return {
        top: anchor.position().top + topPadding,
        left: anchor.position().left,
        width: anchor.outerWidth(),
    };
}

/**
 * @param {*} link
 * @param {*} positionedElement
 * @param {*} popupDimens
 *
 * @returns {{ top: string, left: string, right: string }}
 */
function _calculatePopupPosition(link, positionedElement, popupDimens) {
    let nearestPositionedElement;
    if (HAS_JQUERY) {
        nearestPositionedElement = link.parent().is(positionedElement)
            ? link.parent()
            : positionedElement;
    } else {
        nearestPositionedElement =
            link.parentNode === positionedElement
                ? link.parentNode
                : positionedElement;
    }

    if (_popupFarFromElement(link, nearestPositionedElement)) {
        const anchorPosition = _getPopupAnchorPosition(link);
        return {
            top: anchorPosition.top + "px",
            left:
                Math.round(
                    anchorPosition.left +
                        anchorPosition.width +
                        popupDimens.width / 2
                ) + "px",
            right: "auto",
        };
    }

    return {
        top: link.position().top + "px",
        left: "auto",
        right: "0",
    };
}

function _getClosestPositionedElement(link) {
    var parent = link.parent();
    while (parent && parent.css("position") === "static") {
        if (parent.prop("nodeName") === "HTML") {
            parent = link.parent();
            break;
        }
        parent = parent.parent();
    }
    return parent;
}
