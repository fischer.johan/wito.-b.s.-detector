import { getBrowser } from "../general-utils.js";
import * as ManageBannerModal from "./modal/ManageBannerModal.js";
import { levelToGradient } from "./levels.js";

export { showBanner };

const FEATURE_ISOLATED_STYLING_OF_BANNERS = true;

function showBanner(labelInfos) {
    if (FEATURE_ISOLATED_STYLING_OF_BANNERS) {
        _initBannerByDataAttributeCSS(labelInfos);
    } else {
        _initBannerByJQuery(labelInfos);
    }
}

function _initBannerByDataAttributeCSS(labelInfos) {
    const oldBanner = document.querySelector('div[data-wito="banner"]');
    if (oldBanner) {
        oldBanner.remove();
    }

    if (labelInfos.length === 0) {
        return;
    }

    const gradient = levelToGradient(labelInfos.map(info => info.level));

    const banner = document.createElement("div");
    banner.dataset.wito = "banner";
    document.body.appendChild(banner);

    banner.appendChild(_createMyguardLogo());
    banner.appendChild(_createCategoryText(labelInfos));
    banner.appendChild(_createCloseButton(() => banner.remove()));

    banner.style.background = gradient.background;

    const hasCategoryWithLevelOne = labelInfos.some(item => item.level === 1);
    if (hasCategoryWithLevelOne) {
        banner.style.height = "100px";
    }

    banner.addEventListener("click", () => {
        ManageBannerModal.openModal(labelInfos);
    });
}

function _createMyguardLogo() {
    const myGuardLogo = document.createElement("div");

    myGuardLogo.dataset.wito = "logo";
    myGuardLogo.style.backgroundImage = `url("${getBrowser().extension.getURL(
        "icons/myguard-white.svg"
    )}")`;

    return myGuardLogo;
}

function _createCategoryText(labelInfos) {
    const categories = document.createElement("div");
    categories.dataset.wito = "categories";

    const categoriesName = document.createElement("span");
    categoriesName.dataset.wito = "name";
    categories.appendChild(categoriesName);

    if (labelInfos.length > 1) {
        categoriesName.innerText = labelInfos.map(label => ` ${label.name}`);

        return categories;
    }

    categoriesName.innerText = `${labelInfos[0].name}:`;

    const categoriesMessage = document.createElement("span");
    categories.appendChild(categoriesMessage);
    categoriesMessage.dataset.wito = "message";
    categoriesMessage.innerHTML = ` &nbsp;${labelInfos[0].message}`;

    return categories;
}

function _createCloseButton(onClose) {
    const closeButton = document.createElement("div");

    closeButton.dataset.wito = "close-button";
    closeButton.style.backgroundImage = `url("${getBrowser().extension.getURL(
        "icons/close_white.png"
    )}")`;
    closeButton.addEventListener("click", event => {
        event.stopPropagation();
        onClose();
    });

    return closeButton;
}

function _initBannerByJQuery(labelInfos) {
    $(".wito-alert").remove();

    const labelInfo = labelInfos[0];
    const gradient = levelToGradient(labelInfos.map(info => info.level));

    $("body").parent().append(`<div class="wito-alert"></div>`);
    $(".wito-alert").css(gradient);
    $(".wito-alert").append(
        `<img class="wito-alert-logo-icon" src=${getBrowser().extension.getURL(
            "icons/myguard-white.svg"
        )}></img>`
    );

    const isLevelOne = labelInfos.filter(labelInfo => labelInfo.level === 1);
    if (isLevelOne.length > 1) {
        $(".wito-alert").css("height", "100px");
    }

    if (labelInfos.length > 1) {
        $(".wito-alert").append(
            `<span><b>${labelInfos.map(
                label => ` ${label.name}`
            )}</b> &nbsp;</span>`
        );
    } else {
        $(".wito-alert").append(
            `<span><b>${labelInfo.name}:</b> &nbsp;${labelInfo.message}</span>`
        );
    }

    $(".wito-alert").append(
        `<button class="wito-alert-close wito-button"><img class="wito-close-icon" src=${getBrowser().extension.getURL(
            "icons/close_white.png"
        )}></img></button>`
    );

    $(".wito-alert-close").on("click", () => {
        $(".wito-alert").remove();
    });

    $(".wito-alert").on("click", () => {
        ManageBannerModal.openModal(labelInfos);
    });
}
