import { getBrowser } from "../general-utils.js";

import initWitoAsync from "./initWitoAsync.js";
import * as WitoHTML from "./WitoHTML.js";

const HAS_JQUERY = typeof $ !== "undefined";

main();

function main() {
    if (window !== window.top) {
        return;
    }

    setTimeout(() => {
        WitoHTML.setWindowSize();
        if (HAS_JQUERY) {
            $(window).resize(WitoHTML.setWindowSize);
        } else {
            window.addEventListener("resize", WitoHTML.setWindowSize);
        }
    }, 700);

    initWitoAsync();

    getBrowser().runtime.onMessage.addListener(message => {
        if (message.operation === "languageChange") {
            console.log("Language change: ", message);
            WitoHTML.disconnectMutationObserver();
            initWitoAsync({ clearOldStateFirst: true });
        }
    });
}
