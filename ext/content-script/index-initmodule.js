if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", initAsES6Module);
} else {
    initAsES6Module();
}

/**
 * Idea taken from https://medium.com/@otiai10/how-to-use-es6-import-with-chrome-extension-bd5217b9c978
 */
async function initAsES6Module() {
    const src = _getBrowser().extension.getURL("content-script/index.js");
    await import(src);
}

function _getBrowser() {
    // @ts-ignore
    return typeof chrome === "undefined" ? browser : chrome;
}
