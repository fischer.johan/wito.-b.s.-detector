import { url2Path } from "../../general-utils.js";

import * as BackgroundAPI from "../BackgroundAPI.js";

export default StandardLinkHandler;

function StandardLinkHandler() {
    // @ts-ignore
    this.matches = link => {
        return true;
    };

    this.extract = link => {
        return [url2Path(link)];
    };

    this.linkAPI = links => {
        return BackgroundAPI.getLinkInfo(links);
    };
}
