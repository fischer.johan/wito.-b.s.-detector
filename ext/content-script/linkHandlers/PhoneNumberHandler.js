import * as BackgroundAPI from "../BackgroundAPI.js";

export default PhoneNumberHandler;

const NUMBER_REGEXP = /^tel:(.*)/;

function PhoneNumberHandler() {
    this.matches = link => {
        const numberLink = NUMBER_REGEXP.exec(link);
        return numberLink && numberLink[1];
    };

    this.extract = link => {
        link = NUMBER_REGEXP.exec(link)[1];
        return [decodeURIComponent(link).replace(/\D/g, "")];
    };

    this.linkAPI = links => {
        return BackgroundAPI.getLinkInfo(links);
    };
}
