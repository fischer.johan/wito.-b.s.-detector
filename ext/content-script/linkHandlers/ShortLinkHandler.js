import { url2Domain } from "../../general-utils.js";

import * as BackgroundAPI from "../BackgroundAPI.js";

export default ShortLinkHandler;

function ShortLinkHandler(shorts) {
    this.shortLinkPatterns = shorts;

    this.matches = link => {
        return this.shortLinkPatterns.indexOf(url2Domain(link)) > -1;
    };

    this.extract = link => {
        return [link];
    };

    this.linkAPI = async shortenedLinks => {
        const unshortenedPromises = shortenedLinks.flatMap(async shortLink => {
            const response = await BackgroundAPI.unshortenLinks([shortLink]);
            const links = response.expandedLinks.map(
                expanded => expanded.resolved_url
            );
            return (await BackgroundAPI.getLinkInfo(links)).map(linkInfo => {
                linkInfo.siteId = shortLink;
                return linkInfo;
            });
        });

        return Promise.all(unshortenedPromises).then(response => {
            return response.filter(e => e && e[0]).flatMap(e => e[0]);
        });
    };
}
