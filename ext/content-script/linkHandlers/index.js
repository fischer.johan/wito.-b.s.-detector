import EmailLinkHandler from "./EmailLinkHandler.js";
import FBLinkHandler from "./FBLinkHandler.js";
import LinkHandlerWrapper from "./LinkHandlerWrapper.js";
import PhoneNumberHandler from "./PhoneNumberHandler.js";
import ShortLinkHandler from "./ShortLinkHandler.js";
import StandardLinkHandler from "./StandardLinkHandler.js";

export {
    EmailLinkHandler,
    FBLinkHandler,
    LinkHandlerWrapper,
    PhoneNumberHandler,
    ShortLinkHandler,
    StandardLinkHandler,
};
