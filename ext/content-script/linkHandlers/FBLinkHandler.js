import { url2Path } from "../../general-utils.js";

import * as BackgroundAPI from "../BackgroundAPI.js";

export default FBLinkHandler;

const FB_REGEXP = /^https?:\/\/l\.facebook\.com\/l\.php\?u=([^&]+)/;

function FBLinkHandler() {
    this.matches = link => {
        const fbLink = FB_REGEXP.exec(link);
        return fbLink && fbLink[1];
    };

    this.extract = link => {
        return [this.getFbLink(link)];
    };

    this.getFbLink = link => {
        return url2Path(decodeURIComponent(FB_REGEXP.exec(link)[1]));
    };

    this.linkAPI = links => {
        return BackgroundAPI.getLinkInfo(links);
    };
}
