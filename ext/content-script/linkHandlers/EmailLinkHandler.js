import { emailToDomain, url2Path } from "../../general-utils.js";

import * as BackgroundAPI from "../BackgroundAPI.js";

export default EmailLinkHandler;

const EMAIL_REGEXP = /^mailto:(.*)/;

function EmailLinkHandler() {
    this.matches = link => {
        const emailLink = EMAIL_REGEXP.exec(link);
        return (emailLink && emailLink[1]) || link.includes("@");
    };

    this.extract = link => {
        if (link.includes("@")) {
            return [emailToDomain(link), link];
        } else {
            // @TODO Looks like we have an old bug, here. How can we fix it??
            return [this.getEmailLink(link), EMAIL_REGEXP(link)[1]];
        }
    };

    this.getEmailLink = link => {
        return url2Path(emailToDomain(EMAIL_REGEXP.exec(link)[1]));
    };

    this.linkAPI = links => {
        return BackgroundAPI.getLinkInfo(links);
    };
}
