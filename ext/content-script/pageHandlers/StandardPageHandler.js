import { isSubUrl } from "../../general-utils.js";

import LinkHandlerWrapper from "../linkHandlers/LinkHandlerWrapper.js";

import * as BackgroundAPI from "../BackgroundAPI.js";
import * as WitoHTML from "../WitoHTML.js";

export default StandardPageHandler;

function StandardPageHandler(linkHandlers, siteInfo) {
    this.witoLinkHandlers = linkHandlers.map(
        linkHandler => new LinkHandlerWrapper(linkHandler)
    );

    this.siteInfo = siteInfo;

    this.matches = siteInfo => {
        return true;
    };

    this.filterSubDomainLinks = (siteInfo, links) => {
        return links.filter(
            link =>
                !(siteInfo.siteLabeled && isSubUrl(siteInfo.labeledUrl, link))
        );
    };

    this.getWitoLinks = async links => {
        return BackgroundAPI.getLinkInfo(links);
    };

    this.getLinks = () => {
        const elements = WitoHTML.getLinkElementsNotChecked();
        WitoHTML.setLinkCheckedAttribute(elements, linkElem => [linkElem, ""]);

        for (const element of elements) {
            const matchingHandler = this.witoLinkHandlers.find(handler =>
                handler.matches(element.href)
            );

            if (matchingHandler !== undefined) {
                matchingHandler.add(element, element.href);
            }
        }

        return this.witoLinkHandlers.flatMap(handler =>
            handler.execute(this.siteInfo)
        );
    };
}

/*

        let extractedElements = {};

        for (const element of elements) {
            const matchingHandler = linkHandlers.find(handler => handler.matches(element.href));
            const extractedLinks = await matchingHandler.extract(element.href);
            extractedLinks.forEach(link => {
                WitoHTML.setLinkCheckedAttribute([element], (element) => [element, link]);

                if (link in extractedElements){
                    extractedElements[link].push(element);
                }else{
                    extractedElements[link] = [element];
                }

            });
        }

        console.log("extracted elements", extractedElements)
        console.log("siteinfo", siteInfo)
        const filteredKeys = this.filterSubDomainLinks(this.siteInfo, Object.keys(extractedElements));
        return this.getWitoLinks(filteredKeys).then(labeledLinks => {
            return labeledLinks.flatMap(linkInfo => {
                return extractedElements[linkInfo.siteId]
                    .map(element => ({'wito-info': linkInfo, 'element': element}));
            })
        })

  */

/*return linkHandlers.map(handler => {

            return handler.execute().then(linkElements => (this.filterSubDomainLinks(this.siteInfo, linkElements)))
                .then(async (filteredLinkElements) => {
                    const flaggedWitoLinks = await this.getWitoLinks(filteredLinkElements);
                    const flaggedLinkElements = filteredLinkElements.reduce((res, elem) => {
                        const witoInfo = flaggedWitoLinks.find(link => link.siteId === elem.getAttribute('wito-href') );
                        if(witoInfo !== undefined){
                            res.push({'wito-info': witoInfo, 'element': elem});
                        }
                        return res;
                    }, []);

                    return flaggedLinkElements;

                })
        })*/

/*

    const filterNonWitoLinks = (linkElements, witoLinkInfo) => {
        const typeFilter = (witoLink) => WitoHTML.witoLabels.labels[witoLink.type] !== undefined; //.find(label => label.name === witoLink.type);
        const filteredWitoLinkInfo = witoLinkInfo.filter(typeFilter);
        const linkFilter = (linkElement) => filteredWitoLinkInfo.find(link => link.siteId === linkElement.getAttribute('wito-href')) !== undefined;
        const filteredLinks = linkElements.filter(linkFilter);
        return filteredLinks;
    }

    */
