import * as WitoHTML from "../WitoHTML.js";

export default OutlookPageInfo;

function OutlookPageInfo() {
    this.domain = "outlook.office.com";

    this.links = WitoHTML.getOutlookElements();

    this.extractor = linkElem => [linkElem, linkElem.getAttribute("title")];
}
