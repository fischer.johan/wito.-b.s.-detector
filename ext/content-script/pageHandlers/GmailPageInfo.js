import * as WitoHTML from "../WitoHTML.js";

export default GmailPageInfo;

function GmailPageInfo() {
    this.domain = "mail.google.com";

    this.getLinks = () => WitoHTML.getGmailElements();

    this.extractor = linkElem => [linkElem, linkElem.getAttribute("email")];
}
