import { getBrowser } from "../general-utils.js";

export { levelToIcon, levelToColor, levelToGradient };

const KNOWN_LEVELS = [
    {
        id: "1",
        url: "icons/levels/shield-red.svg",
        color: "#e42f2f",
    },
    {
        id: "2",
        url: "icons/levels/shield-black.svg",
        color: "#000000",
    },
    {
        id: "3",
        url: "icons/levels/shield-yellow.svg",
        color: "#f4b515",
    },
    {
        id: "4",
        url: "icons/levels/shield-blue.svg",
        color: "#319edd",
    },
    {
        id: "5",
        url: "icons/levels/shield-grey.svg",
        color: "#ababab",
    },
    {
        id: "6",
        url: "icons/levels/shield-green.svg",
        color: "#24af18",
    },
    {
        id: "7",
        url: "icons/levels/badge-member.svg",
        color: "#006643",
    },
];

/**
 * @param {string|number} level e.g. 3
 *
 * @returns {string} e.g. "icons/levels/shield-yellow.svg"
 */
function levelToIcon(level) {
    const levelItem = KNOWN_LEVELS.filter(item => item.id === String(level))[0];
    const url = levelItem ? levelItem.url : "icons/levels/shield-grey.svg";
    return getBrowser().extension.getURL(url);
}

/**
 * @param {string|number} level e.g. 3
 * @returns {string} e.g. "#f4b515"
 */
function levelToColor(level) {
    const levelItem = KNOWN_LEVELS.filter(item => item.id === String(level))[0];
    const color = levelItem ? levelItem.color : "#ababab"; // grey
    return color;
}

/**
 * @param {number[]} levels e.g. [1, 3, 3]
 * @returns {object} object with property "background",
 *      e.g. { background: "#ababab" } or
 *           { background: "linear-gradient(90deg, rgb(228, 47, 47), 33.3333%, rgb(0, 0, 0), 66.6667%, rgb(244, 181, 21))" }
 */
function levelToGradient(levels) {
    var occurrences = {};
    levels.forEach(level => {
        if (occurrences[level]) {
            occurrences[level]++;
        } else {
            occurrences[level] = 1;
        }
    });

    // var occurrences = levels.reduce(function (oldResult, level) {
    //     oldResult[level] = (oldResult[level] || 0) + 1;
    //     return oldResult;
    // }, {});

    // e.g. occurrences = { 1: 1, 3: 2 }

    if (Object.keys(occurrences).length === 1) {
        return {
            background: `${levelToColor(levels[0])}`,
        };
    }

    const colorRatio = Object.entries(occurrences).map(([key, value]) => {
        return {
            color: levelToColor(key),
            percentage: (value / levels.length) * 100,
        };
    });

    // e.g. colorRatio = [{ color: "#e42f2f", percentage: 33.33 }, { color: "#f4b515", percentage: 66.67 }]

    const last = colorRatio.pop();
    const gradient = {
        background: `linear-gradient(90deg, ${colorRatio.map(
            ({ color, percentage }, index) =>
                `${color}, ${
                    index === 0
                        ? percentage
                        : percentage + colorRatio[index - 1].percentage
                }%`
        )}, ${last.color})`,
    };

    return gradient;
}
