import { url2Path, getBrowser } from "../general-utils.js";

export {
    getStoredLanguage,
    getDomainInfo,
    getWitoLabels,
    getConfigFile,
    getShorts,
    getLinkInfo,
    unshortenLinks,
};

async function getShorts() {
    console.log("Get short urls");
    const message = {
        operation: "getShorts",
    };
    return await _sendMessageToBackground(message);
}

async function getStoredLanguage() {
    const message = {
        operation: "getStoredValue",
        key: "translation_selected",
    };
    const response = await _sendMessageToBackground(message);
    return response ? response : "sv";
}

async function unshortenLinks(shortLinks) {
    const message = {
        operation: "expandLinks",
        shortLinks: shortLinks,
    };
    return await _sendMessageToBackground(message);
}

async function getDomainInfo(url) {
    url = url2Path(url);
    const response = await getLinkInfo([url]);
    return response.length > 0 ? response[0] : undefined;
}

async function getLinkInfo(links) {
    const message = {
        operation: "getUrlInfo",
        urls: links,
    };
    return await _sendMessageToBackground(message);
}

async function getWitoLabels() {
    const message = {
        operation: "getWitoLabels",
    };
    return await _sendMessageToBackground(message);
}

async function getConfigFile() {
    const message = {
        operation: "getConfigFile",
    };
    return await _sendMessageToBackground(message);
}

async function _sendMessageToBackground(message) {
    return new Promise(resolve => {
        return getBrowser().runtime.sendMessage(null, message, null, resolve);
    });
}
