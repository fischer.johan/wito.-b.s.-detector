import { getBrowser } from "../../general-utils.js";

import * as WitoHTML from "../WitoHTML.js";

export { insertModal, insertBannerModal, removeModal, removeBannerModal };

function insertModal(labelInfo, onClose, popupPosition, popupWidth) {
    $("body").append(`<dialog class="wito-modal"></dialog>`);

    const modalElement = $(`.wito-modal`);
    _initModal(modalElement, labelInfo, onClose);

    const positionCss = _calcPosition(modalElement, popupPosition, popupWidth);

    modalElement.css(positionCss);
}

function insertBannerModal(labelInfo, onClose) {
    $("body").append(`<dialog class="wito-banner-modal"></dialog>`);
    const modalElement = $(`.wito-banner-modal`);
    _initModal(modalElement, labelInfo, onClose);
}

function removeBannerModal() {
    $(`.wito-banner-modal`).remove();
}

function removeModal(modal) {
    modal.remove();
}

function _calcPosition(modal, popupPosition, popupWidth) {
    const windowHeight = WitoHTML.getWindowSize().height
        ? WitoHTML.getWindowSize().height
        : Infinity;
    const windowWidth = WitoHTML.getWindowSize().width
        ? WitoHTML.getWindowSize().width
        : Infinity;
    const xPos = _getFittedLeftPos(
        modal.width(),
        windowWidth,
        popupWidth,
        popupPosition.left
    );
    const yPos = _getFittedTopPos(
        modal.height(),
        windowHeight,
        popupPosition.top
    );
    return {
        top: yPos + "px",
        left: xPos + "px",
        right: "auto",
    };
}

function _getFittedLeftPos(modalWidth, windowWidth, popupWidth, popupLeftPos) {
    const distToPopup = modalWidth * 0.08;
    let xPos = popupLeftPos + popupWidth + distToPopup;
    if (xPos < 0) {
        return 0;
    }
    if (xPos + modalWidth > windowWidth) {
        let leftSidePos = popupLeftPos - (modalWidth + distToPopup);
        if (leftSidePos < 0) {
            return Math.max(windowWidth - modalWidth, 0);
        }

        return leftSidePos;
    }

    return xPos;
}

function _getFittedTopPos(modalHeight, windowHeight, popupTopPos) {
    let yPos = popupTopPos - modalHeight / 2;
    if (yPos < 0) {
        return 0;
    }
    if (yPos + modalHeight > windowHeight) {
        return Math.max(windowHeight - modalHeight, 0);
    }
    return yPos;
}

function _initModal(modal, labelInfo, onClose) {
    _addHeader(modal, onClose);
    _addBody(modal, labelInfo);
    _addFooter(modal);
}

function _addHeader(modal, onClose) {
    modal.append(`<header class="wito-modal-header" ></header>`);
    const modalHeader = $(`.wito-modal-header`);
    _addLogoIcon(modalHeader);
    _addCloseButton(modalHeader, onClose);
}

function _addCloseButton(header, onClose) {
    header.append(
        `<button class="wito-button wito-modal-close-button"><img class="wito-modal-close-icon" src=${getBrowser().extension.getURL(
            "../icons/close_black.png"
        )}></img></button>`
    );
    $(".wito-modal-close-button").on("click", onClose);
}

function _addLogoIcon(header) {
    if (WitoHTML.getSetup().edition === "lrf") {
        header.append(
            `<div class="wito-modal-partnership-header"><img class="wito-partner-logo" src=${getBrowser().extension.getURL(
                `icons/editions/${WitoHTML.getSetup().edition}/logo.svg`
            )}></img><img class="wito-poweredby-logo" src=${getBrowser().extension.getURL(
                "icons/myguard-powered-by.svg"
            )}></img></div>`
        );
    } else {
        header.append(
            `<img class="wito-logo-icon" src=${getBrowser().extension.getURL(
                "icons/myguard-black.svg"
            )}></img>`
        );
    }
}

function _addBody(modal, labelInfo) {
    const labels = _getLabels(labelInfo);
    modal.append(labels);
}

function _getLabels(labelInfo) {
    let labelContainer = $(
        `<section class="wito-modal-label-table"></section>`
    );
    labelInfo.sort((label1, label2) => label1.level - label2.level);
    const labels = labelInfo.map(info => {
        const labelRow = $(`<article class="wito-label"></article>`);
        _addLabelIcon(labelRow, info);
        _addLabelMessage(labelRow, info);
        return labelRow;
    });
    labelContainer.append(labels);
    return labelContainer;
}

function _addLabelMessage(labelRow, labelInfo) {
    labelRow.append(
        `<p class="wito-label-message"><strong>${labelInfo.name}. </strong>  ${labelInfo.message} </p>`
    );
}

function _addLabelIcon(labelRow, labelInfo) {
    labelRow.append(
        `<img class="wito-label-icon" src=${_labelToIcon(labelInfo.id)}></img>`
    );
}

function _labelToIcon(label) {
    let url = "";
    try {
        url = WitoHTML.getSetup().categoryToIcon[label];
    } catch (err) {
        url = "icons/shield-grey.svg";
        console.log("Error: ", err);
        console.warn(`No icon was found for category ${label}.`);
    }
    return getBrowser().extension.getURL(url);
}

function _addFooter(modal) {
    modal.append(
        `<footer class="wito-modal-footer"> <p class="wito-footer-text"> For more information, visit <a class="wito-footer-text" href="https://myguard.online" target="_blank"> myguard.online</a> <p/></footer>`
    );
}
