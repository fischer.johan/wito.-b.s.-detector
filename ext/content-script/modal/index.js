import * as ManageBannerModal from "./ManageBannerModal.js";
import * as ManageModal from "./ManageModal.js";
import * as WitoModal from "./WitoModal.js";

export { ManageBannerModal, ManageModal, WitoModal };
