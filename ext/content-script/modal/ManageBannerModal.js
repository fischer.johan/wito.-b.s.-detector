import * as WitoModal from "./WitoModal.js";

export { openModal, closeModal, resetModal };

const Global = {
    isModalOpen: false,
};

const HAS_JQUERY = typeof $ !== "undefined";

function openModal(labelInfo) {
    if (!Global.isModalOpen) {
        if (HAS_JQUERY) {
            $("body").append('<div class="wito-banner-blur"></div>');

            $(".wito-banner-blur").on("click", closeModal);
        } else {
            const div = document.createElement("div");
            div.setAttribute("class", "wito-banner-blur");
            document.body.appendChild(div);

            div.addEventListener("click", closeModal);
        }

        WitoModal.insertBannerModal(labelInfo, closeModal);

        Global.isModalOpen = true;
    }
}

function closeModal() {
    WitoModal.removeBannerModal();

    if (HAS_JQUERY) {
        $(`.wito-banner-blur`).remove();
    } else {
        const divs = document.getElementsByClassName("wito-banner-blur");
        while (divs.length > 0) {
            divs[0].remove();
        }
        // for (let i = 0; i < divs.length; i++) {
        //     divs[i].remove();
        // }
    }

    Global.isModalOpen = false;
}

function resetModal() {
    if (Global.isModalOpen) {
        closeModal();
    }
}
