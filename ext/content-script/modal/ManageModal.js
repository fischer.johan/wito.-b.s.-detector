import { isSamePosition } from "../../general-utils.js";

import * as WitoModal from "./WitoModal.js";

export {
    onPopupMouseEnter,
    onPopupMouseLeave,
    openModal,
    resetModal,
    closeModal,
};

const HAS_JQUERY = typeof $ !== "undefined";

const CLOSE_DELAY = 600;

const Global = {
    isModalOpen: false,
    openModalInfo: {
        activeCloseTimeout: null,
        linkElement: null,
    },
};

function onPopupMouseEnter(linkElement, labelInfo, popupPosition, popupWidth) {
    if (Global.isModalOpen) {
        if (isSamePosition(Global.openModalInfo.linkElement, linkElement)) {
            _clearCloseTimeout();
            return;
        }

        closeModal();
        return;
    }

    openModal(linkElement, labelInfo, popupPosition, popupWidth);
}

function onPopupMouseLeave(linkElement) {
    if (
        Global.isModalOpen &&
        isSamePosition(Global.openModalInfo.linkElement, linkElement)
    ) {
        _resetCloseTimeout();
        return;
    }

    console.warn("This should never happen");
}

function openModal(linkElement, labelInfo, popupPosition, popupWidth) {
    Global.isModalOpen = true;
    Global.openModalInfo.linkElement = linkElement;

    linkElement.addClass("breathing color");

    if (HAS_JQUERY) {
        $("body").append('<div class="wito-blur"></div>');
    } else {
        const div = document.createElement("div");
        div.setAttribute("class", "wito-blur");
        document.body.appendChild(div);
    }

    WitoModal.insertModal(labelInfo, closeModal, popupPosition, popupWidth);

    if (HAS_JQUERY) {
        $(".wito-modal").mouseenter(_clearCloseTimeout);
        $(".wito-modal").mouseleave(closeModal);
    } else {
        const modals = document.getElementsByClassName("wito-modal");
        for (let i = 0; i < modals.length; i++) {
            modals[i].addEventListener("mouseenter", _clearCloseTimeout);
            modals[i].addEventListener("mouseleave", closeModal);
        }
    }
}

function resetModal() {
    if (Global.isModalOpen) {
        closeModal();
    }
}

function closeModal() {
    const prevLinkElement = Global.openModalInfo.linkElement;
    Global.isModalOpen = false;
    _clearCloseTimeout();
    Global.openModalInfo.linkElement = null;
    prevLinkElement.removeClass("breathing color");

    if (HAS_JQUERY) {
        $(".wito-blur").remove();

        WitoModal.removeModal($(".wito-modal"));
    } else {
        const oldBlurDivs = document.getElementsByClassName("wito-blur");
        while (oldBlurDivs.length > 0) {
            oldBlurDivs[0].remove();
        }

        const oldModals = document.getElementsByClassName("wito-modal");
        WitoModal.removeModal(oldModals);
    }
}

function _clearCloseTimeout() {
    const activeTimeout = Global.openModalInfo.activeCloseTimeout;
    if (activeTimeout !== null) {
        clearTimeout(activeTimeout);
        Global.openModalInfo.activeCloseTimeout = null;
    }
}

function _resetCloseTimeout() {
    _clearCloseTimeout();
    const modalTimeout = setTimeout(closeModal, CLOSE_DELAY);
    Global.openModalInfo.activeCloseTimeout = modalTimeout;
}
