// The keys should match the languages in the category/level-database.
const LANGUAGE_MAP = { en: "🇬🇧 English", sv: "🇸🇪 Svenska" };

init();

// @ts-ignore
async function init() {
    let options = document.querySelector("#translations");

    if (options instanceof HTMLSelectElement) {
        options.addEventListener("change", onOptionChange);

        const languages = Object.keys(LANGUAGE_MAP);
        const translation = await getTranslation();

        for (let i = 0; i < languages.length; i++) {
            const opt = document.createElement("option");
            const language = languages[i];
            opt.value = language;
            opt.text = LANGUAGE_MAP[language];
            if (translation === language) {
                opt.selected = true;
            }
            options.add(opt);
        }
        addLogoImg();
    }
}

function addLogoImg() {
    const logoImg = document.querySelector("#popuplogo");
    logoImg.setAttribute(
        "src",
        getBrowser().extension.getURL("../icons/myguard-black.svg")
    );
}

async function getTranslation() {
    const message = {
        operation: "getStoredValue",
        key: "translation_selected",
    };
    const response = await sendMessageToBackground(message);
    return response ? response : "sv";
}

function onOptionChange() {
    let options = document.querySelector("#translations");

    if (options instanceof HTMLSelectElement) {
        console.log("option change");
        const message = {
            operation: "storeValue",
            key: "translation_selected",
            value: options.value,
        };
        sendMessageToBackground(message);
    }
}

// @ts-ignore
function sendMessageToBackground(message) {
    console.log("sending message in popup");
    return new Promise(resolve => {
        return getBrowser().runtime.sendMessage(null, message, null, resolve);
    });
}

function getBrowser() {
    // @ts-ignore
    return typeof chrome === "undefined" ? browser : chrome;
}
