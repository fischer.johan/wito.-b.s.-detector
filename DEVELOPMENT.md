# Hints for developers

## Dependency graph

There is a file ["dependencies.svg" in the project's root folder](https://gitlab.com/fischer.johan/wito.-b.s.-detector/-/blob/master/dependencies.svg) which can be opened e.g. with a browser. It gives an overview on how all JavaScript files are connected to each other by `import` and `export` statements.

### Update

If some source code was changed, it might be good to update the dependency graph, too. The SVG-file is created of NPM-package "Madge" which depends on a global installation of "GraphViz" on your machine.

1.  Ensure that you have installed GraphViz globally, e.g. with commands from section "Graphviz" on https://www.npmjs.com/package/madge
1.  Invoke the following shell-command inside the project's root folder:
    ```sh
    npm run dependency-graph:update
    ```
